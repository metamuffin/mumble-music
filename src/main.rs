#![feature(async_closure)]

use audiopus::{Application, Channels, SampleRate};
use byteorder::ByteOrder;
use bytes::Bytes;
use clap::crate_version;
use clap::App;
use clap::Arg;
use futures::channel::oneshot;
use futures::future::join;
use futures::join;
use futures::SinkExt;
use futures::StreamExt;
use mumble_protocol::control::msgs;
use mumble_protocol::control::ClientControlCodec;
use mumble_protocol::control::ControlPacket;
use mumble_protocol::crypt::ClientCryptState;
use mumble_protocol::voice::VoicePacket;
use mumble_protocol::voice::VoicePacketPayload;
use std::convert::Into;
use std::convert::TryInto;
use std::net::Ipv6Addr;
use std::net::SocketAddr;
use std::net::ToSocketAddrs;
use std::process::Stdio;
use std::thread::spawn;
use std::time::Duration;
use tokio::io::AsyncReadExt;
use tokio::net::TcpStream;
use tokio::net::UdpSocket;
use tokio::process::Command;
use tokio_native_tls::TlsConnector;
use tokio_util::codec::Decoder;
use tokio_util::udp::UdpFramed;

struct BotOptions {
    server_addr: SocketAddr,
    server_host: String,
    accept_invalid_cert: bool,
    volume: f64,
    username: String,
}

async fn connect(options: &BotOptions, crypt_state_sender: oneshot::Sender<ClientCryptState>) {
    // Wrap crypt_state_sender in Option, so we can call it only once
    let mut crypt_state_sender = Some(crypt_state_sender);

    // Connect to server via TCP
    let stream = TcpStream::connect(&options.server_addr)
        .await
        .expect("Failed to connect to server:");
    println!("TCP connected..");

    // Wrap the connection in TLS
    let mut builder = native_tls::TlsConnector::builder();
    builder.danger_accept_invalid_certs(options.accept_invalid_cert);
    let connector: TlsConnector = builder
        .build()
        .expect("Failed to create TLS connector")
        .into();
    let tls_stream = connector
        .connect(&options.server_host, stream)
        .await
        .expect("Failed to connect TLS: {}");
    println!("TLS connected..");

    let (mut sink, mut stream) = ClientControlCodec::new().framed(tls_stream).split();

    let mut msg = msgs::Authenticate::new();
    msg.set_username(String::from(&options.username));
    msg.set_opus(true);
    sink.send(msg.into()).await.unwrap();

    let ping_loop = async move {
        loop {
            println!("ping!");
            sink.send(ControlPacket::Ping(Box::new(msgs::Ping::new())))
                .await
                .unwrap();
            tokio::time::sleep(Duration::from_millis(1000)).await
        }
    };

    let recv_loop = async move {
        println!("Logging in..");
        let mut crypt_state = None;

        while let Some(packet) = stream.next().await {
            match packet.unwrap() {
                ControlPacket::TextMessage(msg) => {
                    println!(
                        "Got message from user with session ID {}: {}",
                        msg.get_actor(),
                        msg.get_message()
                    );
                }
                ControlPacket::CryptSetup(msg) => {
                    // Wait until we're fully connected before initiating UDP voice
                    crypt_state = Some(ClientCryptState::new_from(
                        msg.get_key()
                            .try_into()
                            .expect("Server sent private key with incorrect size"),
                        msg.get_client_nonce()
                            .try_into()
                            .expect("Server sent client_nonce with incorrect size"),
                        msg.get_server_nonce()
                            .try_into()
                            .expect("Server sent server_nonce with incorrect size"),
                    ));
                }
                ControlPacket::ServerSync(_) => {
                    println!("Logged in!");
                    if let Some(sender) = crypt_state_sender.take() {
                        let _ = sender.send(
                            crypt_state
                                .take()
                                .expect("Server didn't send us any CryptSetup packet!"),
                        );
                    }
                }
                ControlPacket::Reject(msg) => {
                    println!("Login rejected: {:?}", msg);
                }
                _ => {}
            }
        }
    };

    join!(ping_loop, recv_loop);
}

async fn play_audio(options: &BotOptions, crypt_state: oneshot::Receiver<ClientCryptState>) {
    let udp_socket = UdpSocket::bind((Ipv6Addr::from(0u128), 0u16))
        .await
        .expect("Failed to bind UDP socket");
    let crypt_state = match crypt_state.await {
        Ok(crypt_state) => crypt_state,
        Err(_) => return,
    };
    println!("UDP ready!");

    let (mut sink, _) = UdpFramed::new(udp_socket, crypt_state).split();
    sink.send((
        VoicePacket::Audio {
            _dst: std::marker::PhantomData,
            target: 0,
            session_id: (),
            seq_num: 0,
            payload: VoicePacketPayload::Opus(Bytes::from([0u8; 128].as_ref()), true),
            position_info: None,
        },
        options.server_addr,
    ))
    .await
    .unwrap();

    let ffmpeg = Command::new("/bin/ffmpeg")
        .stdout(Stdio::piped())
        .stdin(Stdio::inherit())
        .arg("-re")
        .arg("-i")
        .arg("pipe:0")
        .args(&["-f", "s16le"])
        .args(&["-ac", "1"])
        .args(&["-filter:a", format!("volume={}", options.volume).as_str()])
        .arg("pipe:1")
        .spawn()
        .unwrap();
    let mut pipe = ffmpeg.stdout.unwrap();

    let ms_buf_size = 100;
    let sample_rate = SampleRate::Hz48000;
    let samples = sample_rate as usize * ms_buf_size / 1000;

    let bandwidth = 192000;
    let opus_buf_size = bandwidth / 8 * ms_buf_size / 1000;

    let mut pcm_raw_buf: Vec<u8> = vec![0u8; samples * 2];
    let mut pcm_buf = vec![0i16; samples];
    let mut opus_buf = vec![0u8; opus_buf_size];

    let encoder =
        audiopus::coder::Encoder::new(sample_rate, Channels::Mono, Application::Audio).unwrap();

    let mut seq_num = 0;
    loop {
        pipe.read_exact(&mut pcm_raw_buf).await.unwrap();

        for i in 0..samples {
            pcm_buf[i] =
                byteorder::LittleEndian::read_i16(&[pcm_raw_buf[i * 2], pcm_raw_buf[i * 2 + 1]])
        }

        let len = encoder.encode(&pcm_buf, &mut opus_buf).unwrap();

        sink.send((
            VoicePacket::Audio {
                _dst: std::marker::PhantomData,
                target: 0,
                session_id: (),
                seq_num,
                position_info: None,
                payload: VoicePacketPayload::Opus(Bytes::copy_from_slice(&opus_buf[..len]), false),
            },
            options.server_addr,
        ))
        .await
        .unwrap();
        seq_num += 1;
    }
}

fn app() -> App<'static, 'static> {
    clap::App::new("mumble-music")
        .author("metamuffin <metamuffin@metamuffin.org>")
        .version(crate_version!())
        .before_help("")
        .arg(
            Arg::with_name("host")
                .short("h")
                .long("host")
                .takes_value(true)
                .required_unless_one(&["licence", "gen-completion"]),
        )
        .arg(
            Arg::with_name("port")
                .short("p")
                .takes_value(true)
                .long("port")
                .default_value("64738"),
        )
        .arg(
            Arg::with_name("accept-invalid-cert")
                .short("C")
                .long("accept-invalid-cert"),
        )
        .arg(
            Arg::with_name("volume")
                .short("v")
                .takes_value(true)
                .long("volume")
                .default_value("0.1"),
        )
        .arg(
            Arg::with_name("username")
                .short("u")
                .long("username")
                .takes_value(true)
                .default_value("vewy_gud_music"),
        )
        .arg(
            Arg::with_name("licence")
                .short("L")
                .long("licence")
                .help("Throws a full copy of the AGPL in your face."),
        )
        .arg(
            Arg::with_name("gen-completion")
                .long("generate-completion")
                .help("Generates auto-completion scripts for fish only rn"),
        )
}

#[tokio::main]
async fn main() {
    let args = app().get_matches();

    if args.is_present("licence") {
        println!("{}", &include_str!("../LICENCE"));
        return;
    } else if args.is_present("gen-completion") {
        app().gen_completions(
            "mumble-music",
            clap::Shell::Fish,
            "/usr/share/fish/completions",
        );
        return;
    }

    let server_host = args.value_of("host").unwrap().to_string();
    let server_port = args.value_of("port").unwrap().parse().unwrap();

    let server_addr = (server_host.as_ref(), server_port)
        .to_socket_addrs()
        .expect("Failed to parse server address")
        .next()
        .expect("Failed to resolve server address");

    let (crypt_state_sender, crypt_state_receiver) = oneshot::channel::<ClientCryptState>();

    let options = BotOptions {
        server_addr,
        server_host,
        accept_invalid_cert: args.is_present("accept-invalid-cert"),
        volume: args.value_of("volume").unwrap().parse().unwrap(),
        username: args.value_of("username").unwrap().to_string(),
    };

    join!(
        connect(&options, crypt_state_sender),
        play_audio(&options, crypt_state_receiver)
    );
}
