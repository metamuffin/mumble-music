# mumble-music

A simple headless client for mumble for playing back audio.

## Usage

Audio data is read in the opus format from standart input.

```
mumble-music 0.1.0
metamuffin <metamuffin@metamuffin.org>

USAGE:
    mumble-music [FLAGS] [OPTIONS] --host <host>

FLAGS:
    -C, --accept-invalid-cert    
        --generate-completion    Generates auto-completion scripts for fish only rn
        --help                   Prints help information
    -L, --licence                Throws a full copy of the AGPL in your face.
    -V, --version                Prints version information

OPTIONS:
    -h, --host <host>            
    -p, --port <port>             [default: 64738]
    -u, --username <username>     [default: vewy_gud_music]
    -v, --volume <volume>         [default: 0.1]
```

## Example

`cat music.opus | mumble-music -h example.org -v 0.2`

## Installation

### Commands for Arch

```sh
pacman -S --needed rustup                # requires root
rustup default nightly
git clone https://codeberg.org/metamuffin/muble-music
cd mumble-music
cargo install --path .
mumble-music --generate-completion       # requires root
```
